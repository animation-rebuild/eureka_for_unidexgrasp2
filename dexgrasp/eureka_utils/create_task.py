import yaml

# # Load the YAML file
# task = 'Cartpole'
# suffix = 'GPT'

def create_task(root_dir, task, env_name, suffix):
    # Create task YAML file 
    input_file = "/root/autodl-tmp/projects/LLMDex/UniDexGrasp2/dexgrasp/cfg/shadow_hand_grasp.yaml"
    output_file = "/root/autodl-tmp/projects/LLMDex/UniDexGrasp2/dexgrasp/cfg/shadow_hand_graspgpt.yaml"
    with open(input_file, 'r') as yamlfile:
        data = yaml.safe_load(yamlfile)

    # Modify the "name" field
    data['name'] = f'{task}{suffix}'
    data['env']['env_name'] = f'{env_name}{suffix}'
    
    # Write the new YAML file
    with open(output_file, 'w') as new_yamlfile:
        yaml.safe_dump(data, new_yamlfile)

    # Create training YAML file
    input_file = "/root/autodl-tmp/projects/LLMDex/UniDexGrasp2/dexgrasp/cfg/ppo/config.yaml"
    output_file = "/root/autodl-tmp/projects/LLMDex/UniDexGrasp2/dexgrasp/cfg/ppo/configPPO.yaml"

    with open(input_file, 'r') as yamlfile:
        data = yaml.safe_load(yamlfile)

    #Modify the "name" field
    #data['params']['config']['name'] = data['params']['config']['name'].replace(task, f'{task}{suffix}')

    # Write the new YAML file
    with open(output_file, 'w') as new_yamlfile:
        yaml.safe_dump(data, new_yamlfile)
        